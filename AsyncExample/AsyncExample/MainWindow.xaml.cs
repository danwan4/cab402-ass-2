﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace AsyncExample
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        CancellationTokenSource tokenSource;

        public MainWindow()
        {
            InitializeComponent();
        }

        //private void LoadButton_Click(object sender, RoutedEventArgs e)
        //{
        //    Task<int> calculateNumberTask = LongCalculationTask();
        //    DisplayText("Calculating a number...");
        //    calculateNumberTask.ContinueWith(DisplayNumber,
        //        TaskScheduler.FromCurrentSynchronizationContext());
        //}

        //private void LoadButton_Click(object sender, RoutedEventArgs e)
        //{
        //    Task<int> calculateNumberTask = LongCalculationTask();
        //    DisplayText("Calculating a number...");
        //    calculateNumberTask.ContinueWith(DisplayNumber,
        //        CancellationToken.None,
        //        TaskContinuationOptions.OnlyOnRanToCompletion,
        //        TaskScheduler.FromCurrentSynchronizationContext());
        //    calculateNumberTask.ContinueWith(HandleException,
        //        CancellationToken.None,
        //        TaskContinuationOptions.OnlyOnFaulted,
        //        TaskScheduler.FromCurrentSynchronizationContext());
        //}

        private void LoadButton_Click(object sender, RoutedEventArgs e)
        {
            tokenSource = new CancellationTokenSource();
            Task<int> calculateNumberTask = LongCalculationTask(tokenSource.Token);
            DisplayText("Calculating a number...");
            calculateNumberTask.ContinueWith(DisplayNumber,
                TaskScheduler.FromCurrentSynchronizationContext());
        }

        //private async void LoadButton_Click(object sender, RoutedEventArgs e)
        //{
        //    DisplayText("Calculating a number...");
        //    int number = await LongCalculationTask();
        //    DisplayText(String.Format("Calculated number: {0}", number));
        //}

        //private async void LoadButton_Click(object sender, RoutedEventArgs e)
        //{
        //    DisplayText("Calculating a number...");
        //    try
        //    {
        //        int number = await LongCalculationTask();
        //        DisplayText(String.Format("Calculated number: {0}", number));
        //    }
        //    catch (Exception ex)
        //    {
        //        HandleException(ex);
        //    }
        //}

        private async void LoadButton_Click(object sender, RoutedEventArgs e)
        {
            tokenSource = new CancellationTokenSource();
            DisplayText("Calculating a number...");
            try
            {
                int number = await LongCalculationTask(tokenSource.Token);
                DisplayText(String.Format("Calculated number: {0}", number));
            }
            catch (OperationCanceledException ex)
            {
                HandleCancellation();
            }
            catch (Exception ex)
            {
                HandleException(ex);
            }
        }

        //private void DisplayNumber(Task<int> task)
        //{
        //    int number = task.Result;
        //    DisplayText(String.Format("Calculated number: {0}", number));
        //}

        //private void DisplayNumber(Task<int> task)
        //{
        //    switch (task.Status)
        //    {
        //        case TaskStatus.RanToCompletion:
        //            int number = task.Result;
        //            DisplayText(String.Format("Calculated number: {0}", number));
        //            break;
        //        case TaskStatus.Faulted:
        //            HandleException(task.Exception);
        //            break;
        //    }
        //}

        private void DisplayNumber(Task<int> task)
        {
            switch (task.Status)
            {
                case TaskStatus.RanToCompletion:
                    int number = task.Result;
                    DisplayText(String.Format("Calculated number: {0}", number));
                    break;
                case TaskStatus.Faulted:
                    HandleException(task.Exception);
                    break;
                case TaskStatus.Canceled:
                    HandleCancellation();
                    break;
            }
        }

        private void ResetButton_Click(object sender, RoutedEventArgs e)
        {
            TextOutput.Text = "";
        }

        private void CancelButton_Click(object sender, RoutedEventArgs e)
        {
            tokenSource.Cancel();
        }

        private void DisplayText(String str)
        {
            TextOutput.Text += String.Format("{0}\n", str);
        }

        private void HandleException(Exception ex)
        {
            DisplayText(String.Format("Exception: {0}", ex.Message));
        }

        private void HandleCancellation()
        {
            DisplayText("Calculation has been cancelled.");
        }

        async Task<int> LongCalculationTask(CancellationToken cancellationToken = new CancellationToken())
        {
            await Task.Delay(1000); // Simulating some delay
            cancellationToken.ThrowIfCancellationRequested();
            //throw new NotImplementedException("Method not implemented");
            return 1;
        }
    }
}
